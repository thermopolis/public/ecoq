FREQUENCIES = {
        'mandatory': {
            'mandatory': {
                'Best quality': 24559121,
                'Nominal quality': 1431186,
                'Cloud detected': 4151541,
                'Pixel not produced': 270952,
                },
            },
        'data_quality': {
            'data_quality': {
                'Good quality L1B data': 28494048,
                'Missing stripe pixel in bands 1 and 5': 1647800,
                'Missing/bad L1B data': 270952,
                },
            },
        'iterations': {
            'iterations': {
                'Slow convergence': 270952,
                'Fast convergence': 30141848,
                },
            },
        'atmospheric_opacity': {
            'atmospheric_opacity': {
                '>=3 (Warm, humid air; or cold land)': 272277,
                '0.2 - 0.3 (Nominal value)': 8506047,
                '0.1 - 0.2 (Nominal value)': 21634476,
                },
            },
        'mmd': {
            'mmd': {
                '>0.15 (Most silicate rocks)': 450280,
                '0.1 - 0.15 (Rocks, sand, some soils)': 91452,
                '0.03 - 0.1 (Mostly soils, mixed pixel)': 8586965,
                '<0.03 (Vegetation, snow, water, ice)': 21284103,
                },
            },
        'emissivity_accuracy': {
            'emissivity_accuracy': {
                '>0.02 (Poor performance)': 29498851,
                '0.015 - 0.02 (Marginal performance)': 909839,
                '0.01 - 0.015 (Good performance)': 4110,
                },
            },
        'lst_accuracy': {
            'lst_accuracy': {
                '>2 K (Poor performance)': 4928,
                '1.5 - 2 K (Marginal performance)': 28571704,
                '1 - 1.5 K (Good performance)': 1836168,
                },
            },
        }
