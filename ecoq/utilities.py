from .constants import OUTPUT_FORMAT_DRIVER
from .constants import OUTPUT_COMPRESSOR
from .quality_attributes import CLOUD_OCEAN
from .quality_attributes import ATTRIBUTES
from .cloud_attributes import CLOUD_ATTRIBUTES
import rasterio
import click
import warnings


def read_quality_array(quality_map):
    """
    Import 'quality_map' as a masked Numpy array, using 'rasterio'

    Parameters
    ----------
    quality_map:
        Expectedly an ECOSTRESS QC SDS GTiff

    Returns
    -------
    A masked numpy array

    Examples
    --------
    >>> import rasterio
    >>> qa = read_quality_array('ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif')
    In [165]: qa
    Out[165]:
    (masked_array(
    data=[[16399, 16399, 16399, ..., 16399, 16399, 16399],
            [16399, 16399, 16399, ..., 16399, 16399, 16399],
            [16399, 16399, 16399, ..., 16399, 16399, 16399],
            ...,
            [16399, 16399, 16399, ..., 40975, 40975, 40975],
            [16399, 16399, 16399, ..., 40975, 40975, 40975],
            [16399, 16399, 16399, ..., 40975, 40975, 40975]],
    mask=False,
    fill_value=999999,
    dtype=uint16),
    {'driver': 'GTiff', 'dtype': 'uint16', 'nodata': None, 'width': 5400, 'height': 5632, 'count': 1, 'crs': None, 'transform': Affine(1.0, 0.0, 0.0,
            0.0, 1.0, 0.0), 'tiled': False, 'compress': 'lzw', 'interleave': 'band'})
    """
    # Add some checks!
    # Is it a GDAL-supported format?
    # Is it an ECOSTRESS Quality Control SDS
    # Is it georeferenced?  - Issue warning
    warnings.simplefilter(action='ignore')
    warnings.filterwarnings('ignore')
    with rasterio.open(quality_map) as qm:
        quality_array = qm.read(1, masked=False, out_dtype=rasterio.int32)
        profile = qm.profile
        profile.update(dtype=rasterio.int32)
        profile.update(nodata=-1)
    return quality_array, profile


def get_quality(attribute, integer_flag):
    """
    This function returns the quality (category) of an attribute for a specific
    integer quality flag.
    """
    # FIXME -- use ATTRIBUTES_WITH_VALID_FLAGS in count_attribute_qualities,
    # will result in cleaner code!
    # FIXME -- better: use pydantic models for ATTRIBUTES_WITH_VALID_FLAGS
    # ----------------------------------------------------------------------
    if attribute in set(CLOUD_ATTRIBUTES.attributes.keys()):
        quality = CLOUD_ATTRIBUTES.attributes[attribute].mapping[str(integer_flag)]
    else:
        flag = (bin(integer_flag)[2:].zfill(2))
        if flag not in ATTRIBUTES[attribute]['flags'].keys():
            raise ValueError(f'The requested quality integer flag {integer_flag} does not exist!')
        else:
            quality = ATTRIBUTES[attribute]['flags'][flag]
    return quality


def write_attribute_map(
        attribute_map,
        profile,
        output_filename,
        # driver=OUTPUT_FORMAT_DRIVER,
        compress=OUTPUT_COMPRESSOR,
        binary=False,
        unset_nodata=False,
        ):
    """
    Write a raster map (by default as a) GeoTiff file
    """
    if binary:
        if not unset_nodata:
            profile.update(nodata=0)  # output binary contains only [1]
        else:
            profile.update(nodata=None)  # output binary includes [0, 1]
        profile.update(dtype='uint8')
        profile.update(nbits=1)
    with rasterio.Env():
        with rasterio.open(
                output_filename,
                'w',
                # driver=driver,
                # height=height,
                # width=width,
                # count=1,  # - number of bands?
                # dtype=attribute_map.dtype,
                # nodata=None,  # attribute maps do NOT have a 'nodata' value
                # compress=compress,
                **profile,
             ) as dst:
                dst.write(attribute_map, 1)
