from .utilities import retrieve_subdataset_complete_name
import rasterio

def retrieve_attribute_metadata(attribute, hdf5):
    """
    Parameters
    ----------
    attribute:
        The short name of an ECOSTRESS product subdataset
        (in which case a GDAL raster subdataset using the HDF5 driver)
        whose metadata to retrieve

    hdf5:
        An HDF5 file, presumably an ECOSTRESS product

    Returns
    -------
    A python dictionary listing the metadata of the requested subdataset

    Example
    -------
    >>> from ecor import metadata
    >>> metadata.retrieve_subdataset_metadata('LST', 'ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5')
    ...
    """
    subdataset_name = retrieve_subdataset_complete_name(shortname, hdf5)
    subdataset = rasterio.open(subdataset_name)
    metadata = subdataset.tags(bidx=1)
    return metadata

def retrieve_subdataset_metadata_value(attribute, subdataset, hdf5):
    """
    """
    subdataset_metadata = retrieve_subdataset_metadata(subdataset, hdf5)
    for key in subdataset_metadata:
        return [value for key, value in subdataset_metadata.items() if attribute in key]

