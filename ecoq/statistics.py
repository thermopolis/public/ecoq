from .quality_attributes import CLOUD_OCEAN
from .quality_attributes import ATTRIBUTES
from .extract import extract_attribute
from scipy import stats

def generate_attribute_statistics(
        attribute,
        quality_array,
        axis=None
        ):
    """
    Compute several descriptive statistics for the qualities of the requested
    'attribute' of the passed 'quality_array'

    Parameters
    ----------
    attribute:
        A data quality attribute within an ECOSTRESS QC SDS

    quality_array:
        A masked Numpy array based on an ECOSTRESS QC SDS (GTiff) read using
        'rasterio'

    Returns
    -------
    nobs: int or ndarray of ints:
        Number of observations (length of data along axis). When ‘omit’ is
        chosen as nan_policy, the length along each axis slice is counted
        separately.
    
    minmax: tuple of ndarrays or floats
        Minimum and maximum value of a along the given axis.

    mean: ndarray or float
        Arithmetic mean of a along the given axis.

    variance: ndarray or float
        Unbiased variance of a along the given axis; denominator is number of
        observations minus one.

    skewness: ndarray or float
        Skewness of a along the given axis, based on moment calculations with
        denominator equal to the number of observations, i.e. no degrees of
        freedom correction.

    kurtosis: ndarray or float
        Kurtosis (Fisher) of a along the given axis. The kurtosis is normalized
        so that it is zero for the normal distribution. No degrees of freedom
        are used.
    """
    attribute_array = extract_attribute(attribute, quality_array)
    return stats.describe(attribute_array, axis=None)._asdict()


def generate_statistics(quality_array):
    """
    Compute several descriptive statistics for all attributes of the passed array

    Parameters
    ----------
    quality_array:
        A masked Numpy array based on an ECOSTRESS QC SDS (GTiff) read using
        'rasterio'

    Returns
    -------
        Returns descriptive statistics (nobs, minmax, mean, variance, skewness,
        kurtosis) for all attributes of the passed array. See also
        generate_attribute_statistics().

    Examples
    --------
    >>> from ecoq import api
    >>> quality_map = 'ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif'
    >>> qa, profile = api.read_quality_array(quality_map)
    >>> api.generate_attribute_statistics('mandatory', qa)
        DescribeResult(nobs=30412800, minmax=(0, 3), mean=0.3467988478535354, variance=0.5529970309089809, skewness=1.8544661537744485, kurtosis=1.8442333259293298)
    """
    statistics = {}
    for attribute in ATTRIBUTES:
    # FIXME -- use ATTRIBUTES_WITH_VALID_FLAGS, will result in cleaner code!
    # from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
    # for attribute in ATTRIBUTES_WITH_VALID_FLAGS:
    # ----------------------------------------------------------------------
        if attribute != CLOUD_OCEAN:
            statistics[attribute] = generate_attribute_statistics(attribute, quality_array)
        else:
            statistics[CLOUD_OCEAN] = ATTRIBUTES[CLOUD_OCEAN]['qualities']
    return statistics
