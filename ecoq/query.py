from .quality_attributes import ATTRIBUTES
from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
from .bitpatterns import extract_bitpattern
import shlex
import subprocess


def query_pixel_value(raster, column, row):
    """Query the value of a given pixel location using `gdallocationinfo`

    Parameters
    ----------
    source_file :
        The source raster map file to query

    column :
        The column (or 'x' in GDAL or 'abscissa' in mathematics) in the source
        raster map file to query

    row :
        The row (or 'y' in GDAL or 'ordinate' in mathematics) in the source
        raster map file to query

    Returns
    -------
    pixel_value
        The value of the picture element located at (row, column) pixel
        coordinates

    Examples
    --------
    ..
    """
    command = 'gdallocationinfo -valonly {source} {x} {y}'
    command = command.format(source=raster, x=column, y=row)
    command = shlex.split(command)

    try:
        pixel_value = subprocess.check_output(command)
    except Exception as e:
        print(e.output.decode()) # print out the stdout messages up to the exception
        print(e) # To print out the exception message

    return int(pixel_value)

def query_pixel_quality(attribute, quality_pixel_value):
    """
    """
    bit_positions = ATTRIBUTES[attribute]['bits']
    bitpattern = extract_bitpattern(quality_pixel_value, bit_positions)
    binary_string = (bin(bitpattern)[2:].zfill(2))
    quality = ATTRIBUTES[attribute]['flags'][binary_string]
    return quality

def query_pixel_qualities(quality_pixel_value):
    """
    """
    pixel_qualities = {}
    for attribute in ATTRIBUTES_WITH_VALID_FLAGS:
        pixel_qualities[attribute] = query_pixel_quality(attribute, quality_pixel_value)
    return pixel_qualities
