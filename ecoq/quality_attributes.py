"""
Bit flags defined in the QC SDS for the 5-band ECOSTRESS algorithm (reading
from left to right).
Source: https://ecostress.jpl.nasa.gov/downloads/atbd/ECOSTRESS_L2_ATBD_LSTE_2018-03-08.pdf
"""

MANDATORY = 'mandatory'
DATA_QUALITY = 'data_quality'
CLOUD_OCEAN = 'cloud_ocean'
ITERATIONS = 'iterations'
ATMOSPHERIC_OPACITY = 'atmospheric_opacity'
MMD = 'mmd'
EMISSIVITY_ACCURACY = 'emissivity_accuracy'
LST_ACCURACY = 'lst_accuracy'

ATTRIBUTES_WITH_VALID_FLAGS = {
        MANDATORY,
        DATA_QUALITY,
        ITERATIONS,
        ATMOSPHERIC_OPACITY,
        MMD,
        EMISSIVITY_ACCURACY,
        LST_ACCURACY,
}

titles = {
    MANDATORY: 'Mandatory',  #QA flags',
    DATA_QUALITY: 'Data',  #quality flag',
    CLOUD_OCEAN: 'Cloud/Ocean',  #flag',
    ITERATIONS: 'Iterations',
    ATMOSPHERIC_OPACITY: 'Atmospheric Opacity',
    MMD: 'MMD',
    EMISSIVITY_ACCURACY: 'Emissivity accuracy',
    LST_ACCURACY: 'LST accuracy',
    }

bits = {
    MANDATORY: (1,0),
    DATA_QUALITY: (3,2),
    CLOUD_OCEAN: (5,4),
    ITERATIONS: (7,6),
    ATMOSPHERIC_OPACITY: (9,8),
    MMD: (11,10),
    EMISSIVITY_ACCURACY: (13, 12),
    LST_ACCURACY: (15, 14),
    }

# 0 & 1
"""
Mandatory QA flags
00 = Pixel produced, best quality
01 = Pixel produced, nominal quality. Either one or
more of the following conditions are met:
    1. emissivity in both bands 4 and 5 < 0.95, i.e.
    possible cloud contamination
    2. low transmissivity due to high water vapor
    loading (<0.4), check PWV values and error
    estimates
    3. Pixel falls on missing scan line in bands 1&5, and
    filled using spatial neural net. Check error
    estimates.
    Recommend more detailed analysis of other QC
    information
10 = Pixel produced, but cloud detected
11 = Pixel not produced due to missing/bad data, user
should check Data quality flag bits
"""
mandatory_flags = {
        '00': 'Best quality',
        '01': 'Nominal quality',
        '10': 'Cloud detected',
        '11': 'Pixel not produced',
        }
mandatory_qualities =  {
        'Best quality': '00',
        'Nominal quality': '01',
        'Cloud detected': '10',
        'Pixel not produced': '11',
        }

# 3 & 2
data_quality_flags = {
        '00': 'Good quality L1B data',
        '01': 'Missing stripe pixel in bands 1 and 5',
        '10': 'Unset',
        '11': 'Missing/bad L1B data',
        }
data_quality_qualities = {
        'Good quality L1B data':                 '00',
        'Missing stripe pixel in bands 1 and 5': '01',
        'Unset':                                 '10',
        'Missing/bad L1B data':                  '11',
        }

# 5 & 4
cloud_ocean_flags = {
        'Not set': 'Please check ECOSTRESS GEO and CLOUD products for this information.',
        }
cloud_ocean_qualities = {
        'Not set': 'Please check ECOSTRESS GEO and CLOUD products for this information.',
        }

# 7 & 6
iterations_flags = {
        '00': 'Slow convergence',
        '01': 'Nominal',
        '10': 'Nominal',
        '11': 'Fast convergence',
        }
iterations_qualities = {
        'Slow convergence':'00',
        'Nominal':         '01',
        'Nominal':         '10',
        'Fast convergence':'11',
        }

# 9 & 8
atmospheric_opacity_flags = {
        '00': '>=3 (Warm, humid air; or cold land)',
        '01': '0.2 - 0.3 (Nominal value)',
        '10': '0.1 - 0.2 (Nominal value)',
        '11': '<0.1 (Dry, or high altitude pixel)',
        }
atmospheric_opacity_qualities = {
        '>=3 (Warm: humid air; or cold land)': '00',
        '0.2 - 0.3 (Nominal value)':           '01',
        '0.1 - 0.2 (Nominal value)':           '10',
        '<0.1 (Dry: or high altitude pixel)':  '11',
        }

# 11 & 10
mmd_flags = {
        '00': '>0.15 (Most silicate rocks)',
        '01': '0.1 - 0.15 (Rocks, sand, some soils)',
        '10': '0.03 - 0.1 (Mostly soils, mixed pixel)',
        '11': '<0.03 (Vegetation, snow, water, ice)',
        }
mmd_qualities = {
        '>0.15 (Most silicate rocks)':            '00',
        '0.1 - 0.15 (Rocks, sand, some soils)':   '01',
        '0.03 - 0.1 (Mostly soils, mixed pixel)': '10',
        '<0.03 (Vegetation, snow, water, ice)':   '11',
        }

# 13 & 12
emissivity_accuracy_flags = {
        '00': '>0.02 (Poor performance)',
        '01': '0.015 - 0.02 (Marginal performance)',
        '10': '0.01 - 0.015 (Good performance)',
        '11': '<0.01 (Excellent performance)',
        }
emissivity_accuracy_qualities = {
        '>0.02 (Poor performance)':            '00',
        '0.015 - 0.02 (Marginal performance)': '01',
        '0.01 - 0.015 (Good performance)':     '10',
        '<0.01 (Excellent performance)':       '11',
        }

# 15 & 14
lst_accuracy_flags = {
        '00': '>2 K (Poor performance)',
        '01': '1.5 - 2 K (Marginal performance)',
        '10': '1 - 1.5 K (Good performance)',
        '11': '<1 K (Excellent performance)',
        }
lst_accuracy_qualities = {
        '>2 K (Poor performance)':          '00',
        '1.5 - 2 K (Marginal performance)': '01',
        '1 - 1.5 K (Good performance)':     '10',
        '<1 K (Excellent performance)':     '11',
        }

ATTRIBUTES = {
        MANDATORY: {
            'title': titles[MANDATORY],
            'bits': bits[MANDATORY],
            'flags': mandatory_flags,
            'qualities': mandatory_qualities,
            },
        DATA_QUALITY: {
            'title': titles[DATA_QUALITY],
            'bits': bits[DATA_QUALITY],
            'flags': data_quality_flags,
            'qualities': data_quality_qualities,
            },
        CLOUD_OCEAN: {
            'title': titles[CLOUD_OCEAN],
            'bits': bits[CLOUD_OCEAN],
            'flags': cloud_ocean_flags,
            'qualities': cloud_ocean_qualities,
            },
        ITERATIONS: {
            'title': titles[ITERATIONS],
            'bits': bits[ITERATIONS],
            'flags': iterations_flags,
            'qualities': iterations_qualities,
            },
        ATMOSPHERIC_OPACITY: {
            'title': titles[ATMOSPHERIC_OPACITY],
            'bits': bits[ATMOSPHERIC_OPACITY],
            'flags': atmospheric_opacity_flags,
            'qualities': atmospheric_opacity_qualities,
            },
        MMD: {
            'title': titles[MMD],
            'bits': bits[MMD],
            'flags': mmd_flags,
            'qualities': mmd_qualities,
            },
        EMISSIVITY_ACCURACY: {
            'title': titles[EMISSIVITY_ACCURACY],
            'bits': bits[EMISSIVITY_ACCURACY],
            'flags': emissivity_accuracy_flags,
            'qualities': emissivity_accuracy_qualities,
            },
        LST_ACCURACY: {
            'title': titles[LST_ACCURACY],
            'bits': bits[LST_ACCURACY],
            'flags': lst_accuracy_flags,
            'qualities': lst_accuracy_qualities,
            },
        }
