expected_output_for_list_attributes = [
        ([], "  mandatory\n  data_quality\n  cloud_ocean\n  iterations\n  atmospheric_opacity\n  mmd\n  emissivity_accuracy\n  lst_accuracy\n"),
        (['mandatory'], {'mandatory': {'00': 'Best quality', '01': 'Nominal quality', '10': 'Cloud detected', '11': 'Pixel not produced'}}),
        (['data_quality'], {'data_quality': {'00': 'Good quality L1B data', '01': 'Missing stripe pixel in bands 1 and 5', '10': 'Unset', '11': 'Missing/bad L1B data'}}),
        (['cloud_ocean'], {'cloud_mask': {'0': 'Not determined', '1': 'Determined'}, 'final_cloud_plus_region_growing': {'0': 'No', '1': 'Yes'}, 'final_cloud': {'0': 'No', '1': 'Yes'}, 'test_band_4': {'0': 'No', '1': 'Yes'}, 'test_band_5': {'0': 'No', '1': 'Yes'}, 'land_water_mask': {'0': 'Land', '1': 'Water'}}),
        (['iterations'], {'iterations': {'00': 'Slow convergence', '01': 'Nominal', '10': 'Nominal', '11': 'Fast convergence'}}),
        (['atmospheric_opacity'], {'atmospheric_opacity': {'00': '>=3 (Warm, humid air; or cold land)', '01': '0.2 - 0.3 (Nominal value)', '10': '0.1 - 0.2 (Nominal value)', '11': '<0.1 (Dry, or high altitude pixel)'}}),
        (['mmd'], {'mmd': {'00': '>0.15 (Most silicate rocks)', '01': '0.1 - 0.15 (Rocks, sand, some soils)', '10': '0.03 - 0.1 (Mostly soils, mixed pixel)', '11': '<0.03 (Vegetation, snow, water, ice)'}}),
        (['emissivity_accuracy'], {'emissivity_accuracy': {'00': '>0.02 (Poor performance)', '01': '0.015 - 0.02 (Marginal performance)', '10': '0.01 - 0.015 (Good performance)', '11': '<0.01 (Excellent performance)'}}),
        (['lst_accuracy'], {'lst_accuracy': {'00': '>2 K (Poor performance)', '01': '1.5 - 2 K (Marginal performance)', '10': '1 - 1.5 K (Good performance)', '11': '<1 K (Excellent performance)'}})
        ]
