from .quality_attributes import ATTRIBUTES
from .cloud_attributes import CLOUD_ATTRIBUTES

def extract_bitpattern(decimal, positions):
    """
    Extracts the bitpattern from a 'decimal' located between
    the requested 'positions'

    Parameters
    ----------

    decimal:
        Decimal value from whose binary form to extract the requested bitpattern

    positions:
        A tuple listing the positional indices of bits in a bitpattern.
        The function computes internally the length of the requested
        sub-bitpattern based on the number of indices given.
        Indexing bit positions starts at 0.

    Returns
    -------

    A integer

    Examples
    --------
    >>> extract_bitpattern(44742, (15,14))
    2

    Another example
    >>> extract_bitpattern(16399, (3,2))
    3

    Another example with more bits
    >>> extract_bitpattern(16399, (3,2,1,0))
    15
    """
    selector = int(len(positions) * '1', 2)  # which bits?
    starting_bit = min(positions)  # smallest index points to starting bit
    return (((decimal >> starting_bit) & selector))


def integerize_attribute_binary(attribute_binary):
    """
    This functions converts the input 'attribute_binary' string into an integer
    and performs a bitwise left shift by the number of 'bits' reserved for the
    attribute in question

    Parameters
    ----------
    attribute_binary:
        An 'attribute_binary' string of the form '<attribute>:<binary>' where:
            - 'attribute' may be any of the valid attributes hardcoded in
              ATTRIBUTES_WITH_VALID_FLAGS

            - 'binary' may be any of the binary quality flags for an attribute
              hardcoded in ATTRIBUTES_WITH_VALID_FLAGS[attribute]['flags']

            Examples to explain the bitwise math performed here-in:

            The "quality" integer form of the binary string '10' shifted to the
            left by 14 is 32768.
            The binary '11' shifted to the left by 12 is 12288.

    Returns
    -------
    An integer that encodes the requested attribute and quality flag

    Examples
    --------
    ..
    """
    attribute, binary = attribute_binary.split(':')
    if attribute in set(CLOUD_ATTRIBUTES.attributes.keys()):
        shifting = CLOUD_ATTRIBUTES.attributes[attribute].bit
    else:
        shifting = min(ATTRIBUTES[attribute]['bits'])
    return int(binary, 2) << shifting


def integerize_attribute_selector(attribute):
    """
    This function computes an integer that acts as a selector for the bit
    positions of an attribute among the ones hardcoded in
    ATTRIBUTES_WITH_VALID_FLAGS

    Parameters
    ----------
    attribute

    Returns
    -------
    An integer that selects the bit positions, for the requested attribute,
    in a 16-bit encoded integer
    """
    if attribute in set(CLOUD_ATTRIBUTES.attributes.keys()):
        bits = CLOUD_ATTRIBUTES.attributes[attribute].bit
        shifting = bits
    else:
        bits = ATTRIBUTES[attribute]['bits']
        shifting = min(bits)
    positions = len(str(bits))
    selector = int(positions * '1', 2)  # which bits?
    return selector << shifting
