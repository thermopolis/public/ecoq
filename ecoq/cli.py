from .messages import MESSAGE_QUALITY_ATTRIBUTES
import functools
import click
from pathlib import Path
from .quality_attributes import ATTRIBUTES
from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
from .query import query_pixel_value
from .query import query_pixel_qualities
from . import api
import pprint


@click.group()
def cli():
    pass


def common_options(f):
    options = [
        click.argument('quality_map',
                        type=click.Path(
                            exists=True,
                            dir_okay=False,
                        )
        ),
        click.argument('attribute',
                       required=True,
                       nargs=-1
        ),
        click.option("-o",
                     "--output",
                     type=click.Path(
                         exists=False,
                         dir_okay=False,
                         resolve_path=True),
                     help="Output file name (without .tif extension)",
        ),
        click.option("-d",
                     "--output-directory",
                     type=click.Path(
                         exists=False,
                         dir_okay=True,
                         resolve_path=True),
                     help="Output directory",
        ),
        click.option("-q",
                     "--quiet",
                     is_flag=True,
                     default=False,
                     help="Suppress progress bars and messages",
        ),
    ]
    return functools.reduce(lambda x, opt: opt(x), options, f)


@cli.command('list')
@click.argument('attribute', required=False, nargs=-1)
@click.option('-c', '--complete', is_flag=True, default=False, help="Print complete list of quality attributes and flags")
@click.option("-p", "--pretty-print", is_flag=True, default=False, help="Print in a format easier to read, i.e. with columns with headers")
def list_quality_attributes(attribute, complete=False, pretty_print=False):
    """List data quality attributes"""
    quality_attributes = api.list_attributes(
            attributes=attribute,
            complete=complete,
    )
    if not attribute and pretty_print:
        click.echo(MESSAGE_QUALITY_ATTRIBUTES)

    if type(quality_attributes) == str:
        click.echo(f'{quality_attributes}')

    if type(quality_attributes) == dict:
        for quality, flags in quality_attributes.items():
            click.echo('')  # empty line between and in case of multiple qualities
            click.echo(f'{quality}:', color=True)
            for flag, description in flags.items():
                click.echo(f'  {flag}: {description}')

    if type(quality_attributes) == list:
        for quality in quality_attributes:
            click.echo(f'  {quality}')

@cli.command('count')
# @common_options
@click.argument('attribute', required=False, nargs=-1)
@click.argument('quality_map',
        type=click.Path(
            exists=True,
            dir_okay=False,
            )
        )
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
@click.option("-p", "--pretty-print", is_flag=True, default=False, help="Print in a format easier to read, i.e. with columns with headers")
def count_attribute_qualities(
        quality_map,
        attribute,
        all_attributes=False,
        output=None,
        output_directory=None,
        pretty_print=False,
        ):
    """
    Count attribute quality classes
    """
    if not attribute or all_attributes or 'all' in set(attribute):
        attribute = 'all'

    frequencies = api.count_qualities(attribute, quality_map)

    for attribute, flags in frequencies.items():
        if pretty_print:  # print only if requested
            click.echo('')  # empty line between and in case of multiple qualities
            string = 'Frequency'.rjust(9) + f" '{attribute}' quality"
            string += '\n' + '-' * len('Frequency') + f" {'-' * (2 + len(attribute) + len(' quality'))}"
            click.echo(f'{string}')
        # numbers first, see: https://stackoverflow.com/a/62756126/1172302
        for flag, frequency in flags.items():
            click.echo(f'{frequency:9} \'{flag}\'')


@cli.command('statistics')
# @common_options
@click.argument('attribute',
                required=False,
                nargs=-1,
                type=click.Choice(
                    ['mandatory',
                     'data_quality',
                     'cloud_ocean',
                     'iterations',
                     'atmospheric_opacity',
                     'mmd',
                     'emissivity_accuracy',
                     'lst_accuracy'],
                    case_sensitive=False)
                )
@click.argument('quality_map',
        type=click.Path(
            exists=True,
            dir_okay=False,
            )
        )
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
@click.option("-p", "--pretty-print", is_flag=True, default=False, help="Print in a format easier to read, i.e. with columns with headers")
def generate_attribute_statistics(
        quality_map,
        attribute,
        all_attributes=False,
        output=None,
        output_directory=None,
        pretty_print=False,
        quiet=False,
        ):
    """
    Generate descriptive statistics for attribute qualities
    """
    if not attribute or all_attributes or 'all' in set(attribute):
        attribute = 'all'
    statistics = api.generate_quality_statistics(
            attributes=attribute,
            quality_map=quality_map,
    )
    for attribute, descriptors in statistics.items():
        click.echo('')  # empty line between and in case of multiple qualities
        if pretty_print:
            click.echo(f'{attribute}:')
        click.echo(f"Number of observations: {descriptors['nobs']}")
        click.echo(f"Minimum: {descriptors['minmax'][0]}")
        click.echo(f"Maximum: {descriptors['minmax'][1]}")
        click.echo(f"Mean: {descriptors['mean']}")
        click.echo(f"Variance: {descriptors['variance']}")
        click.echo(f"Skewness: {descriptors['skewness']}")
        click.echo(f"Kurtosis: {descriptors['kurtosis']}")


@cli.command('query')
@click.argument('attribute', required=False, nargs=-1)
@click.argument('quality_map', type=click.Path(exists=True, dir_okay=False))
@click.argument('ordinate', type=click.IntRange(min=0))
@click.argument('abscissa', type=click.IntRange(min=0))
@click.option("-b", "--print-binary", is_flag=True, default=False, help="Print the binarized decimal")
@click.option("-s", "--suppress-qualities", is_flag=True, default=False, help="Suppress printing qualities")
@click.option("-a", "--all-attributes", is_flag=True, default=False, help="Extract all QA variables in separate GTiff files")
@click.option("-q", "--quiet", is_flag=True, default=False, help="Suppress progress bars and messages")
def query_quality_value(
        quality_map,
        attribute,
        abscissa,
        ordinate,
        all_attributes=False,
        print_binary=False,
        suppress_qualities=False,
        quiet=False,
        ):
    """Query a quality control pixel at (x, y)"""
    # if attribute not in set(ATTRIBUTES):
    #     error = "The requested attribute '{a}' does not exist!".format(a=attribute)
    #     raise ValueError(error)
    quality_pixel_value = query_pixel_value(
                                            raster=quality_map,
                                            column=abscissa,
                                            row=ordinate,
                                            )

    message = int('{integer}'.format(integer=quality_pixel_value))
    if not quiet:
        message = "Pixel value: " + str(message)

    if print_binary:
        binary = (bin(quality_pixel_value)[2:])
        message += " [{binary}]".format(binary=binary)
    click.echo(pprint.pformat(message))

    if not suppress_qualities:
        pixel_qualities = query_pixel_qualities(quality_pixel_value)

        from .quality_attributes import CLOUD_OCEAN
        if CLOUD_OCEAN in set(attribute):
            click.echo(ATTRIBUTES[CLOUD_OCEAN]['qualities'])
            return

        if all_attributes or 'all' in set(attribute) or not attribute:
            attribute = ATTRIBUTES_WITH_VALID_FLAGS

        # output reads nicer with 'title's!
        pixel_quality = {
                ATTRIBUTES[item]['title']: pixel_qualities[item]
                for item in attribute
                }
        if not quiet:
            return click.echo(pprint.pformat(pixel_quality, indent=2))
        else:
            for quality in pixel_quality.values():
                click.echo(quality)


@cli.command('filter')
@click.argument('quality', required=True, nargs=-1)
@click.argument('quality_map', type=click.Path(exists=True, dir_okay=False))
@click.option("-o", "--output", type=click.Path(exists=False, dir_okay=False, resolve_path=True), help="Output file name",)
@click.option("-b", "--binary-output", is_flag=True, default=False, help="Ouput binary maps with '1 = requested quality attribute")
@click.option("-n", "--unset-nodata", is_flag=True, default=False, help="Ouput binary maps with '0 = No Data' and '1 = requested quality attribute. Flag functional with '-b'.")
@click.option("-u", "--print-unique-values", is_flag=True, default=False, help="Print unique integer values")
@click.option("-q", "--quiet", is_flag=True, default=False, help="Suppress progress bars and messages")
# @common_options
def filter_quality_map(
        quality_map,
        quality,
        output=None,
        binary_output=False,
        unset_nodata=False,
        print_unique_values=False,
        quiet=False,
        ):
    """
    Filter quality attributes based on user requested flags
    """
    filtered_quality_attribute = api.filter_quality_attribute(
            quality_map,
            quality,
            output,
            binary_output,
            unset_nodata,
            print_unique_values,
            quiet,
    )
    if print_unique_values:
        click.echo(f"Unique quality integer values: {filtered_quality_attribute}")


@cli.command('extract')
# @common_options
@click.argument('attribute', required=False, nargs=-1)
@click.argument('quality_map', type=click.Path(exists=True, dir_okay=False))
@click.option("-o",
        "--output",
        type=click.Path(exists=False,
            dir_okay=False,
            resolve_path=True),
        help="Output file name (without .tif extension)",
        )
@click.option("-d", "--output-directory", type=click.Path(exists=False, dir_okay=True, resolve_path=True), help="Output directory")
@click.option('-p', '--prefix', default='', help='Prefix the output filename')
@click.option('-a', '--all-attributes', is_flag=True, default=False, help='Extract all QA variables in separate GTiff files')
def extract_quality_attributes(
        quality_map,
        attribute,
        prefix=None,
        output=None,
        all_attributes=False,
        output_directory=None,
        quiet=False,
        ):
    """Extract quality attribute in individual raster map"""
    api.extract_attributes(
             quality_map,
             attributes=attribute,  # note attributes vs. attribute
             prefix=prefix,
             output=output,
             all_attributes=all_attributes,
             output_directory=output_directory,
             )
    # check if raster map(s) are written?
    if 'all' in set(attribute):
        attributes = ATTRIBUTES_WITH_VALID_FLAGS
    else:
        attributes = attribute
    if not quiet:
        for element in attributes:
            if output:
                output = f'{Path(output).stem}.tif'
            message = "'{attribute}' quality flags extracted to '{output}'"
            attribute = ATTRIBUTES[element]['title']
            if not output:
                output=f'{element}.tif'
            if prefix:
                output = prefix + '_' + output
            click.echo(message.format(attribute=attribute, output=output), err=True)
