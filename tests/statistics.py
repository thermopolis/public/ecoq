import math

STATISTICS = {
        'mandatory': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 0.3467988478535354,
            'variance': 0.5529970309089809,
            'skewness': 1.8544661537744485,
            'kurtosis': 1.8442333259293298,
            },
        'data_quality': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 0.08090856481481482,
            'variance': 0.1278172342682779,
            'skewness': 5.759152980349596,
            'kurtosis': 38.96463757393101,
            },
        'cloud_ocean': {
            'nobs': 30412800,
            'minmax': (0, 0),
            'mean': 0.0,
            'variance': 0.0,
            'skewness': math.nan,
            'kurtosis': math.nan,
            },
        'iterations': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 2.9732725694444446,
            'variance': 0.07946793873553651,
            'skewness': -10.452426721635442,
            'kurtosis': 107.25322437115537,
            },
        'atmospheric_opacity': {
            'nobs': 30412800,
            'minmax': (0, 2),
            'mean': 1.7024081636679294,
            'variance': 0.22693636414396243,
            'skewness': -1.1317419740674952,
            'kurtosis': -0.10953386079691052,
            },
        'mmd': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 2.667222057817761,
            'variance': 0.3168844895057089,
            'skewness': -1.9785943918115614,
            'kurtosis': 5.336206721430269,
            },
        'emissivity_accuracy': {
            'nobs': 30412800,
            'minmax': (0, 2),
            'mean': 0.03018659906355219,
            'variance': 0.029545650206359332,
            'skewness': 5.571324398319017,
            'kurtosis': 29.647820843716353,
            },
        'lst_accuracy': {
                'nobs': 30412800,
                'minmax': (0, 2),
                'mean': 1.0602128051346802,
                'variance': 0.056911299177861675,
                'skewness': 3.6616947195638847,
                'kurtosis': 11.607449278748453,
                }
        }

STATISTICS_FOR_GENERATE_QUALITY_STATISTICS = {
        'mandatory': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 0.3467988478535354,
            'variance': 0.5529970309089809,
            'skewness': 1.8544661537744485,
            'kurtosis': 1.8442333259293298,
            },
        'data_quality': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 0.08090856481481482,
            'variance': 0.1278172342682779,
            'skewness': 5.759152980349596,
            'kurtosis': 38.96463757393101,
            },
        'iterations': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 2.9732725694444446,
            'variance': 0.07946793873553651,
            'skewness': -10.452426721635442,
            'kurtosis': 107.25322437115537,
            },
        'atmospheric_opacity': {
            'nobs': 30412800,
            'minmax': (0, 2),
            'mean': 1.7024081636679294,
            'variance': 0.22693636414396243,
            'skewness': -1.1317419740674952,
            'kurtosis': -0.10953386079691052,
            },
        'mmd': {
            'nobs': 30412800,
            'minmax': (0, 3),
            'mean': 2.667222057817761,
            'variance': 0.3168844895057089,
            'skewness': -1.9785943918115614,
            'kurtosis': 5.336206721430269,
            },
        'emissivity_accuracy': {
            'nobs': 30412800,
            'minmax': (0, 2),
            'mean': 0.03018659906355219,
            'variance': 0.029545650206359332,
            'skewness': 5.571324398319017,
            'kurtosis': 29.647820843716353,
            },
        'lst_accuracy': {
                'nobs': 30412800,
                'minmax': (0, 2),
                'mean': 1.0602128051346802,
                'variance': 0.056911299177861675,
                'skewness': 3.6616947195638847,
                'kurtosis': 11.607449278748453,
                }
        }

ATTRIBUTES_AND_STATISTICS = {
        'mandatory': {
            'mandatory': {
                'nobs': 30412800,
                'minmax': (0, 3),
                'mean': 0.3467988478535354,
                'variance': 0.5529970309089809,
                'skewness': 1.8544661537744485,
                'kurtosis': 1.8442333259293298,
                },
            },
        'data_quality': {
            'data_quality': {
                'nobs': 30412800,
                'minmax': (0, 3),
                'mean': 0.08090856481481482,
                'variance': 0.1278172342682779,
                'skewness': 5.759152980349596,
                'kurtosis': 38.96463757393101,
                },
            },
        'cloud_ocean': {
            'cloud_ocean': {
                'nobs': 30412800,
                'minmax': (0, 0),
                'mean': 0.0,
                'variance': 0.0,
                'skewness': math.nan,
                'kurtosis': math.nan,
                },
            },
        'iterations': {
            'iterations': {
                'nobs': 30412800,
                'minmax': (0, 3),
                'mean': 2.9732725694444446,
                'variance': 0.07946793873553651,
                'skewness': -10.452426721635442,
                'kurtosis': 107.25322437115537,
                },
            },
        'atmospheric_opacity': {
            'atmospheric_opacity': {
                'nobs': 30412800,
                'minmax': (0, 2),
                'mean': 1.7024081636679294,
                'variance': 0.22693636414396243,
                'skewness': -1.1317419740674952,
                'kurtosis': -0.10953386079691052,
                },
            },
    'mmd': {
            'mmd': {
                'nobs': 30412800,
                'minmax': (0, 3),
                'mean': 2.667222057817761,
                'variance': 0.3168844895057089,
                'skewness': -1.9785943918115614,
                'kurtosis': 5.336206721430269,
                },
            },
    'emissivity_accuracy': {
            'emissivity_accuracy': {
                'nobs': 30412800,
                'minmax': (0, 2),
                'mean': 0.03018659906355219,
                'variance': 0.029545650206359332,
                'skewness': 5.571324398319017,
                'kurtosis': 29.647820843716353,
                },
            },
    'lst_accuracy': {
            'lst_accuracy': {
                'nobs': 30412800,
                'minmax': (0, 2),
                'mean': 1.0602128051346802,
                'variance': 0.056911299177861675,
                'skewness': 3.661694719563884,
                'kurtosis': 11.607449278748453,
                }
            }
    }
