## `ecoq`

`ecoq` (read _**ECO**STRESS **q**uality_) assissts the use of Quality Control
Scientific Data Sets (QC SDS) in [ECOSTRESS] Temperature (T) and ε (Emissivity)
[LSTE][] products.

[LSTE]: https://ecostress.jpl.nasa.gov/downloads/psd/ECOSTRESS_SDS_PSD_L2_ver1-1.pdf "Level 2 Product Specification Document"

<!-- [QC SDS]: **Q**uality **C**ontrol **S**cientific **D**ata **S**et -->

>[ECOSTRESS][] (**ECO**system **S**paceborne **T**hermal **R**adiometer
>**E**xperiment on **S**pace **S**tation) **is a** thermal infrared
>experimental **mission** mounted on the [ISS][]'
>[Kibo][] module. Its scan is perpendicular to the ISS velocity.
>
><figure>
>    <img
>        src="images/ECOSTRESS-In-Container.png" height=180
>        alt="ECOSTRESS radiometer in container (credit: NASA/JPL)"
>    />
>    <figcaption>
>        <a href="https://ecostress.jpl.nasa.gov/mission/ECOSTRESSInContainer.png/view">ECOSTRESS
>        radiometer in container (credit: NASA/JPL)</a>
>    </figcaption>
></figure>

> Jump straight to the [Install](#install) section
or read the following examples
to get a taste of what `ecoq` can do


Data quality attributes report on surface, atmospheric, and sensor conditions.
They enable users to obtain a summary of algorithm statistics in a spatial
context. Consequently they may assist in determining the usefulness of
observations.

Pixels in a QC SDS contain unsigned integers that represent bit-packed
combinations of various retrieval conditions such as surface type (land or
ocean), atmospheric conditions (i.e., water vapor content: dry, moist, very
humid, etc.), and cloud cover (and in the case of LSTE products, overall
performance of the TES algorithm).

`ecoq` can selectively:

- `list` data quality attributes and flags
- `extract` quality attributes in raster maps
- `query` a quality control pixel at (x, y)
- `count` frequencies of quality flags per attribute
- generate descriptive `statistics`  for attribute qualities
- `filter` quality attributes based on user requested flags

[ECOSTRESS]: https://ecostress.jpl.nasa.gov/ "ECOSTRESS"
[ECOSTRESS mission]: https://ecostress.jpl.nasa.gov/mission "ECOSTRESS Mission"
[ISS]: https://en.wikipedia.org/wiki/International_Space_Station "Internaltional Space Station"
[Kibo]: https://en.wikipedia.org/wiki/Kibo_(ISS_module) "Kibo (ISS module)"


## Examples

`ecoq` features several subcommands.
Executing
`ecoq --help`
or even `ecoq`
will present subcommands,
optional flags
and common arguments.

### `ecoq list`

First,
one can list the quality _categories_
(also and simply referred to as _qualities_)
for a single data quality attribute with the `list` subcommand.
For example,
```bash
ecoq list Mandatory
```
will return
```bash
mandatory:
  00: Best quality
  01: Nominal quality
  10: Cloud detected
  11: Pixel not produced
```

We are presented with
4 qualities for the 'mandatory' attribute.
Likewise,
all other attributes feature 4 quality categories.


> Note also that the above example shows that
even capitalised attribute strings
(in which case '_Mandatory_')
are handled gracefully by `ecoq`.

### `ecoq extract`

Extract all qualities of an attribute
in a separate GeoTiff raster map:
```bash
ecoq extract mmd ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
'MMD' quality flags extracted to 'mmd.tif'
```

or all attributes in separate GeoTiff raster maps in one go:
```bash
 ecoq extract all ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
'Mandatory' quality flags extracted to 'mandatory.tif'
'Data' quality flags extracted to 'data_quality.tif'
'Cloud/Ocean' quality flags extracted to 'cloud_ocean.tif'
'Iterations' quality flags extracted to 'iterations.tif'
'Atmospheric Opacity' quality flags extracted to 'atmospheric_opacity.tif'
'MMD' quality flags extracted to 'mmd.tif'
'Emissivity accuracy' quality flags extracted to 'emissivity_accuracy.tif'
'LST accuracy' quality flags extracted to 'lst_accuracy.tif'
```

The above can be tasked also via the `-a` option like
```
ecoq extract -a ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
```
which and however will be silent (currently).

> Examples of extracted quality attributes over Milano (2019, DOY 049, 08:16:06)

<!-- ![Atmospheric opacity][atmospheric_opacity_over_milano] -->
<!-- ![Data quality][data_quality_over_milano] -->
<!-- ![Emissivity accuracy][emissivity_accuracy_over_milano] -->
<!-- ![Iterations][iterations_over_milano] -->
<!-- ![LST accuracy][lst_accuracy_over_milano] -->
<!-- ![Mandatory][mandatory_over_milano] -->
<!-- ![MMD][mmd_over_milano] -->

<div>
<p float="center">
<img alt="" src="images/milano_atmospheric_opacity_doy2019049081606.png" width="250" />
<img alt="" src="images/milano_data_quality_doy2019049081606.png" width="250" />
<img alt="" src="images/milano_emissivity_accuracy_doy2019049081606.png" width="250" />
<img alt="" src="images/milano_iterations_doy2019049081606.png" width="250" />
<img alt="" src="images/milano_lst_accuracy_doy2019049081606.png" width="250" />
<img alt="" src="images/milano_mandatory_doy2019049081606.png" width="250" />
<img alt="" src="images/milano_mmd_doy2019049081606.png" width="250" />
</p>
</div>

### `ecoq query`

The following example
querying a single pixel
from a Quality Control SDS via
```bash
ecoq query ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif 1000 1000
```
will return
```bash
'Pixel value: 20165'
{ 'Atmospheric Opacity': '0.1 - 0.2 (Nominal value)',
  'Data': 'Missing stripe pixel in bands 1 and 5',
  'Emissivity accuracy': '>0.02 (Poor performance)',
  'Iterations': 'Fast convergence',
  'LST accuracy': '1.5 - 2 K (Marginal performance)',
  'MMD': '<0.03 (Vegetation, snow, water, ice)',
  'Mandatory': 'Nominal quality'}
```

Queries,
as almost every subcommand,
can target one or multiple attributes.
For example
```bash
ecoq query mmd ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif 1000 1000
'Pixel value: 20165'
{'MMD': '<0.03 (Vegetation, snow, water, ice)'}
```
and
```bash
ecoq query mmd iterations ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif 1000 1000
'Pixel value: 20165'
{ 'Iterations': 'Fast convergence',
  'MMD': '<0.03 (Vegetation, snow, water, ice)'}
```

### `ecoq count`

Counting the frequency of qualities
for the `mandatory` attribute via
```bash
ecoq count mandatory ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
```
will return
```bash
  Frequency 'mandatory'
   38130880 'Best quality'
    2028414 'Nominal quality'
    6990549 'Cloud detected'
   26514990 'Pixel not produced'
```

### `ecoq statistics`

Generate descriptive statistics of qualities
for the `mandatory` attribute via
```bash
ecoq statistics mandatory ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif
```
will return
```bash
  mandatory:
    Number of observations: 30412800
    Minimum value: 0
    Maximum value: 3
    Mean: 0.3467988478535354
    Variance: 0.5529970309089809
    Skewness: 1.8544661537744485
    Kurtosis: 1.8442333259293298
```

### `ecoq filter`

Filtering data quality attribute
for user-requested quality flags via
```
ecoq filter mmd:00 ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC_Compressed.tif -o mmd0.tif
```
will write the requested `mmd0.tif` file
that contains only the `mmd` data quality attribute
and only observations flagged with the binary quality flag 00.

In which case, it is
```
 ecoq list mmd |ag 00
{'mmd': {'00': '>0.15 (Most silicate rocks)',
```

Multiple qualities can be retrieved too!
```
ecoq filter mmd:00 mmd:01 ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC_Compressed.tif -u -o mmd_00_01.tif
```

Note the `-u` option will print unique integer values for the requested
qualities.

### More filtering examples
Filtering is somehow flexible -- think about various options of post-processing!

```
ecoq filter mandatory:10 ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif -o cloud.tif
```
or,
using the `-b` (`--binary-output`) flag
`ecoq` will write out `0` for No Data cells
and `1` for the requested quality:
```
ecoq filter mandatory:10 ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif -o cloud_binary_map.tif -b
```
or using in addition the `-n` (`--unset-nodata`) flag
there will be no NoData value set!
```
ecoq filter mandatory:10 ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC.tif -o cloud_binary_map_nodata_unset.tif -b -n
```

All three examples
will output the same map
in different formats
(data types and raster map categories).

- The first one will contain the value `2` for cloudy pixels.
- The second one will containt only `1` for cloudy pixels -- that's it.
  And it'll look identically to the first one.
- The third one will contain `1` for cloudy pixels and `0` for any other pixel of the image. So all `other` pixels won't be transparent as before.

> Maps below rendered on a black background.

![cloudy pixels (Int32)][cloudy-int32]
<!-- ![cloudy pixels (single value)][cloudy-single-value] -->
![cloudy pixels (binary map)][cloudy-binary]

[cloudy-int32]: images/clouds.png
<!-- [cloudy-single-value]: images/clouds_binary_map.png -->
[cloudy-binary]: images/clouds_binary_map_nodata_unset.png

> Review and update the following!
>In case of multiple qualities,
>the output map follows the following rule:
>`0` for No Data;
>`1` for '`00`';
>`1` for '`01`';
>`2` for '`10`'
>and `3` for '`11`'.


## Install

`ecoq` strives to grow [up] and become a proper Python package.
It is recommended to install it inside a virtual environment.

```
python -m venv ecoq_virtualenv
source ecoq_virtualenv/bin/activate
pip install -U pip
pip install git+https://gitlab.com/thermopolis/public/ecoq
```
or if asked, add the `--user` option, i.e.
```
pip install -U pip --user
pip install git+https://gitlab.com/thermopolis/public/ecoq --user
```


## Python API

`ecoq` features an API


```
ecoq.list_attributes
ecoq.count_qualities
ecoq.generate_quality_statistics
ecoq.filter_quality_attribute
ecoq.extract_attributes
```

[**Update API Documentation**]

##

> `ecoq` is developed within the activities of the [THERMOPOLIS]() research project
>
> `ecoq` is inspired more or less by similar projects:
>   - https://github.com/mapbox/rio-l8qa
>   - https://github.com/haoliangyu/pymasker
>   - https://gitlab.com/NikosAlexandris/i.landsat8.qa

## Resources

- [Answer to a MODIS QC bitmask related question in GIS.SE](https://gis.stackexchange.com/a/351728/5256)

## Acknowledgments

Tips & Hints while building `ecoq` by:

- https://gitlab.com/matteopcc
- https://gitlab.com/pmav99
