import pathlib
import pytest
from ecoq.api import (
        list_attributes,
        count_qualities,
        generate_quality_statistics,
        filter_quality_attribute,
        extract_attributes,
        )
from ecoq.utilities import read_quality_array
from tests.test_parameters import expected_output_for_list_attributes
from tests.attributes import ATTRIBUTES_FOR_COUNT_QUALITIES
from tests.filenames import GEOTIFF_FILENAME
from tests.frequencies import FREQUENCIES
from tests.statistics import STATISTICS_FOR_GENERATE_QUALITY_STATISTICS


@pytest.mark.parametrize(
    "attributes,expected_output",
    expected_output_for_list_attributes,
)
def test_list_attributes(attributes, expected_output):
    assert list_attributes(attributes) == expected_output

@pytest.fixture
def attributes():
    return ATTRIBUTES_FOR_COUNT_QUALITIES

@pytest.fixture(params=ATTRIBUTES_FOR_COUNT_QUALITIES)
def attribute(request):
    return [request.param]

@pytest.fixture
def quality_map():
    geotiff = pathlib.Path('tests/input/') / GEOTIFF_FILENAME
    if not geotiff.exists():
        pytest.skip(f"Test file {GEOTIFF_FILENAME} does not exist: {geotiff.as_posix()}")
    return geotiff

@pytest.fixture
def quality_array():
    geotiff = pathlib.Path('tests/input/') / GEOTIFF_FILENAME
    if not geotiff.exists():
        pytest.skip(f"Test file {GEOTIFF_FILENAME} does not exist: {geotiff.as_posix()}")
    quality_array, array_profile = read_quality_array(geotiff)
    return quality_array

def test_count_qualities(attribute, quality_map):
    """
    """
    expected_output = FREQUENCIES[attribute[0]]
    assert count_qualities(attribute, quality_map) == expected_output


def test_generate_quality_statistics(attributes, quality_map):
    """
    """
    expected_output = STATISTICS_FOR_GENERATE_QUALITY_STATISTICS
    assert generate_quality_statistics(attributes, quality_map) == expected_output


def test_filter_quality_attribute():
    """
    """
    pass


def test_extract_attributes():
    """
    """
    pass
