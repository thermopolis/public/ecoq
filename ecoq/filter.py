from .bitpatterns import integerize_attribute_binary
from .bitpatterns import integerize_attribute_selector
import numpy

def filter_requested_quality(quality_array, attribute_binary, binary_output):
    """
    This function filters an ECOSTRESS QC SDS and retains the requested
    quality flags

    Parameters
    ----------
    quality_array:
        An ECOSTRESS QC SDS raster map

    attribute_binary
        A string in the form of 'attribute:binary', composed by a quality
        'attribute' and a 'binary' indicating the quality level for the specific
        attribute. Consult the ECOSTRESS LSTE Product Specification
        manual or the ATTRIBUTES dictionary defined in `quality_attributes.py`.

    binary_output:
        A boolean flag. If True, output a binary quality attribute map (with 0
        for No Data and 1 for the requested quality)

    Returns
    -------
        A numpy array containing the requested quality integer pixel value

    Examples
    --------
    >>> import rasterio
    >>> geotiff = rasterio.open("ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC_Compressed.tif")
    >>> quality = geotiff.read(1,)
    >>> filter_requested_quality(quality, ['mandatory:00'])
    array([0, 0, 0, ..., 0, 0, 0], dtype=...)
    """
    quality_integer = integerize_attribute_binary(attribute_binary)
    attribute = attribute_binary.split(':')[0]
    masking_integer = integerize_attribute_selector(attribute)

    quality_array = quality_array & masking_integer
    boolean_quality_array = quality_array != quality_integer  # =! to build a masked array
    requested_quality_array = numpy.ma.masked_array(quality_array, boolean_quality_array, fill_value=-1)
    requested_quality_array = requested_quality_array.filled(-1)

    if binary_output:
        requested_quality_array = (~boolean_quality_array).astype(numpy.ubyte)
    #     shifting = min(ATTRIBUTES[attribute]['bits'])
    #     wanted = list(ATTRIBUTES[attribute]['flags'].keys())[1]
    #     min_quality_integer = int(wanted, 2) << shifting
    #     requested_quality_array = (requested_quality_array // min_quality_integer) + 1
    #     requested_quality_array = requested_quality_array.astype(bool) * 1
    #     requested_quality_array = requested_quality_array.astype('uint8')


    return requested_quality_array


def filter_requested_qualities(quality_array, requested_qualities, binary_output):
    """
    This function filters an ECOSTRESS QC SDS and retains the requested
    quality flags

    Parameters
    ----------
    quality_array:
        An ECOSTRESS QC SDS raster map

    requested_qualities
        A list of "quality" integers. Consult the ECOSTRESS LSTE Product
        Specification manual or the ATTRIBUTES dictionary defined in
        `quality_attributes.py`.

    binary_output:
        A boolean flag. If True, output a binary quality attribute map (with 0
        for No Data and 1 for the requested quality)

    Returns
    -------
        A numpy array containing the requested quality integer pixel values

        [
         # A boolean numpy array containing 'True' for the requested quality
         # integer pixel values
        ]

    Examples
    --------
    >>> import rasterio
    >>> geotiff = rasterio.open("ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC_Compressed.tif")
    >>> quality = geotiff.read(1,)
    >>> filter_requested_qualities(quality, ['mandatory:00'])
    array([0, 0, 0, ..., 0, 0, 0], dtype=uint16)
    """
    requested_quality_arrays = []
    for attribute_binary in requested_qualities:
        requested_quality_array = filter_requested_quality(
                quality_array,
                attribute_binary,
                binary_output,
                )
        requested_quality_arrays.append(requested_quality_array)
    return requested_quality_arrays

