ATTRIBUTES = [
        'mandatory',
        'data_quality',
        'cloud_ocean',
        'iterations',
        'atmospheric_opacity',
        'mmd',
        'emissivity_accuracy',
        'lst_accuracy',
]
ATTRIBUTES_FOR_COUNT_QUALITIES = [
        'mandatory',
        'data_quality',
        'iterations',
        'atmospheric_opacity',
        'mmd',
        'emissivity_accuracy',
        'lst_accuracy',
]
