from .constants import OUTPUT_NODATA
from .constants import OUTPUT_NODATA_NONE
from .constants import OUTPUT_EXTENSION
from .quality_attributes import ATTRIBUTES
from .quality_attributes import ATTRIBUTES_WITH_VALID_FLAGS
from .cloud_attributes import CLOUD_ATTRIBUTES
from .count import count_attribute_qualities
from .count import count_all_qualities
from .statistics import generate_attribute_statistics
from .statistics import generate_statistics
import rasterio
import numpy
from .query import query_pixel_value
from .extract import extract_attribute
from .extract import extract_quality_attribute  # for 'cloud_ocean' only
from .utilities import read_quality_array
from .utilities import write_attribute_map
from .filter import filter_requested_qualities


def cloud_attributes_dictionary(attributes_dictionary):
    """
    Convert an instance of the CloudAttributes class to a nested dictionary structure.

    Parameters
    ----------
    attributes_dictionary : CloudAttributes
        The attributes dictionary to be converted.

    Returns
    -------
    dict
        A nested dictionary containing qualitative attributes for the
        'cloud_ocean' attribute along with the corresponding flags and
        qualities. The keys of the outer dictionary are the attribute IDs, and
        the values are inner dictionaries. The keys of the inner dictionaries
        are the flags, and the values are the corresponding descriptions.

    Examples
    --------
    >>> result = list_attributes_dictionary(CLOUD_ATTRIBUTES)
    >>> print(result)
    {
        'cloud_mask': {
            '0': 'Not determined',
            '1': 'Determined'
        },
        'final_cloud_plus_region_growing': {
            '0': 'No',
            '1': 'Yes'
        },
        ...
    }
    """
    quality_attributes = {}
    for attribute, flags_and_descriptions in attributes_dictionary.attributes.items():
        quality_attributes[attribute] = {}
        for flag, description in flags_and_descriptions.mapping.items():
            quality_attributes[attribute][flag] = description
    return quality_attributes


def list_attributes(attributes, complete=False):
    """
    List the requested data quality attributes from the ATTRIBUTES dictionary

    Parameters
    ----------
    attributes : list
        A list of attributes among the keys hardcoded in the dictionary ATTRIBUTES.
        See also quality_attributes.py.

    complete : bool, optional
        A boolean flag indicating whether to list attributes along with their
        qualities and quality flags or just the attributes. If True, the function will
        list attributes along with their qualities and quality flags. If False
        (default), only the attributes will be listed.

    Returns
    -------
    quality_attributes: dict or str
        If `complete` is True, the dictionary ATTRIBUTES in which the keys of
        the dictionary are the attributes, and the values are dictionaries
        pairing qualities and flags for each attribute.

        If `complete` is False and no specific attribute is requested, a string
        containing a list of all quality attributes without listing their
        qualities and quality flags.

        If `complete` is False and one or more specific attributes are
        requested, one or more dictionaries containing the requested attributes
        along with their qualities and quality flags.


    Examples
    --------
    ..
    """
    quality_attributes = {}
    try:
        if complete:  # list attributes and qualities
            for attribute in ATTRIBUTES:
                title = ATTRIBUTES[attribute]['title']
                qualities = ATTRIBUTES[attribute]['qualities']
                quality_attributes[title] = qualities
            return quality_attributes

        if not attributes:  # list only attributes
            quality_attributes = str()
            for attribute in ATTRIBUTES.keys():
                quality_attributes += f'  {attribute}\n'
            return quality_attributes

        if attributes:
            attributes = (string.lower() for string in attributes)  # thus accept capitalised strings
            for attribute in attributes:
                if attribute == 'cloud_ocean':
                    quality_attributes = cloud_attributes_dictionary(CLOUD_ATTRIBUTES)
                else:
                    quality_attributes[attribute] = ATTRIBUTES[attribute]['flags']
            return quality_attributes
    except KeyError:
        return f"'{attribute}' is not a valid quality attribute name!"


def count_qualities(attribute, quality_map):
    """
    Count the number of times each unique quality class of the
    requested data quality 'attribute' appears in the 'quality_map'.

    Parameters
    ----------
    attribute:
        A data quality attribute within an ECOSTRESS QC SDS

    quality_map:
        An ECOSTRESS QC SDS

    Returns
    -------
    A dictionary with frequency statistics for qualities (quality categories)
    of data quality attributescontained in an ECOSTRESS QC SDS
    """
    quality_array, array_profile = read_quality_array(quality_map)

    if attribute == 'all':
        frequencies = count_all_qualities(quality_array)

    else:
        frequencies = {}
        for item in attribute:
            frequencies[item] = count_attribute_qualities(item, quality_array)

    return frequencies


def generate_quality_statistics(attributes, quality_map):
    """
    """
    quality_array, array_profile = read_quality_array(quality_map)

    if attributes == 'all':
        statistics = generate_statistics(quality_array)

    else:
        statistics = {}
        for attribute in attributes:
            statistics[attribute] = generate_attribute_statistics(attribute, quality_array)

    return statistics


def filter_quality_attribute(
        quality_map,
        quality,
        output,
        binary_output,
        unset_nodata,
        print_unique_values,
        quiet,
        ):
    """
    Filter a Quality Control Science Data Set (here, the 'quality_map') for the
    requested 'quality'

    Parameters
    ----------
    quality_map:
        An ECOSTRESS QC SDS

    quality:
        A specific data quality attribute (and class) in the form of a composite
        string <attribute:binary_quality_flag>, where:
        - attribute is any of the hardcoded attributes in
          ATTRIBUTES_WITH_VALID_FLAGS
        - binary_quality_flag is any of the corresponding binary flags to the
          requested attribute, as hardcoded in ATTRIBUTES_WITH_VALID_FLAGS

    output:
        The output filename to write the filtered quality map

    binary_output:
        A boolean flag wether to output binary quality attribute maps (possible
        values 0 or 1 respectively for No Data and the requested quality
        attribute)

    print_unique_values:
        A boolean flag whether to print unique quality integer values that...

    quiet:
        A boolean flag to adjust verbosity

    Returns
    -------
    This function either writes a map containing the requested filtered values,
    thus it does not return any data structure, or it returns the output of
    filter_requested_qualities()

    Examples
    --------
    >>> from ecoq import api
    >>> api.filter_quality_attribute(
            quality_map='ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_QC_Compressed.tif',
            quality=['mandatory:00'],
            output=None,
            binary_output=False,
            print_unique_values=True,
            quiet=False
        )
    array([    0, 65535], dtype=uint16)
    """
    quality_array, array_profile = read_quality_array(quality_map)
    array_profile.update(transform=rasterio.transform.guard_transform(array_profile['transform']))
    filtered_quality_arrays = filter_requested_qualities(
                                                        quality_array=quality_array,
                                                        requested_qualities=quality,
                                                        binary_output=binary_output,
                              )

    if not len(filtered_quality_arrays) > 1:
        filtered_quality_array = filtered_quality_arrays[0]

    else:
        # for nodata=-1 -> sum = -1 * len(filtered_quality_arrays)
        # ...a feature counting stacked layers?
        filtered_quality_array = sum(filtered_quality_arrays)

    if output:
        # output_filename = quality[0] + OUTPUT_EXTENSION
        write_attribute_map(
                attribute_map=filtered_quality_array,
                profile=array_profile,
                output_filename=output,
                binary=binary_output,
                unset_nodata=unset_nodata,
        )

    if print_unique_values:
        unique_quality_values = numpy.unique(filtered_quality_array)
        return unique_quality_values


def extract_attributes(
        quality_map,
        attributes,
        prefix,
        output,
        all_attributes=False,
        output_directory=None,
    ):
    """
    Extract the requested data quality attribute from an ECOSTRESS QC SDS in a
    separate (georeferenced?) raster map

    Parameters
    ----------
    quality_map:
        An ECOSTRESS QC SDS

    attributes:
        One or multiple data quality attribute(s) to extract from an ECOSTRESS
        QC SDS

    output:
        Output filename for the extracted data quality attribute

    all_attributes:
        Boolean flag, if True all attributes are extracted each in a separate
        raster map

    output_directory:
        Output directory for the extracted data quality attribute raster map(s)

    Returns
    -------
    This function writes a map containing the requested quality attributes,
    yet does not return any data structure.
    The quality attribute values are integerized attribute binaries.

    For example, the attribute 'mmd' features the following qualities:
        - >0.15 (Most silicate rocks)':            00
        - 0.1 - 0.15 (Rocks, sand, some soils)':   01
        - 0.03 - 0.1 (Mostly soils, mixed pixel)': 10
        - <0.03 (Vegetation, snow, water, ice)':   11

    Extracting the 'mmd' attribute from an ECOSTRESS QC SDS, the output pixel
    values, will be: 0 (for 00), 1 (for 01), 2 (for 10) and 3 (for 11)
    """

    quality_array, array_profile = read_quality_array(quality_map)
    array_profile.update(nodata=OUTPUT_NODATA_NONE)
    array_profile.update(transform=rasterio.transform.guard_transform(array_profile['transform']))

    if all_attributes or 'all' in set(attributes):
        attributes = ATTRIBUTES_WITH_VALID_FLAGS  # will not include 'cloud_ocean'

    for attribute in attributes:

        # for cloud/ocean, use extract_quality_attribute()
          # to feed a different 'attributes_dictionary'
        if attribute in set(CLOUD_ATTRIBUTES.attributes.keys()):
            attribute_map = extract_quality_attribute(
                    attributes_dictionary=CLOUD_ATTRIBUTES,
                    attribute=attribute,
                    quality_array=quality_array,
            )
            array_profile.update(dtype='uint8')
        else:
            attribute_map = extract_attribute(attribute, quality_array)
            #
            # Add a selector flag to keep output attribute quality values
            # integerized?  For example: for 'mmd', 1 should be 1024, 2 should be
            # 2048 and 3 should be 3096
            #
            shifting = min(ATTRIBUTES[attribute]['bits'])
            integerized_attribute_array = attribute_map << shifting

        output = attribute
        if prefix:
            import os
            output = prefix + '_' + os.path.basename(output)
        extension = OUTPUT_EXTENSION
        output_filename = f"{output}.{extension}"

        write_attribute_map(
                attribute_map=attribute_map,
                profile=array_profile,
                output_filename=output_filename,
        )
