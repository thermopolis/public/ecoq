import pydantic
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple

class CloudAttribute(pydantic.BaseModel):
    id: str
    title: str
    bit: int
    mapping: Dict[str, str]

    class Config:
        allow_mutation = False

class CloudAttributes(pydantic.BaseModel):
    id: str
    title: str
    attributes: dict

    class Config:
        allow_mutation = False

NO_YES = {
        '0': 'No',
        '1': 'Yes',
        }

CLOUD_MASK = CloudAttribute(
        id='cloud_mask',
        title='Cloud Mask',
        bit=0,
        mapping={
            '0': 'Not determined',
            '1': 'Determined',
        }
)

FINAL_CLOUD_PLUS_REGION_GROWING = CloudAttribute(
        id='final_cloud_plus_region_growing',
        title='Final cloud plus region growing',
        bit=1,
        mapping=NO_YES
)

FINAL_CLOUD = CloudAttribute(
        id='final_cloud',
        title='Final cloud (either of bits 3 or 4 set)',
        bit=2,
        mapping=NO_YES
)

TEST_BAND_4 = CloudAttribute(
        id='test_band_4',
        title='Band 4 brightness threshold test',
        bit=3,
        mapping=NO_YES
)

TEST_BAND_5 = CloudAttribute(
        id='test_band_5',
        title='Band 4-5 thermal difference test',
        bit=4,
        mapping=NO_YES,
)

LAND_WATER_MASK = CloudAttribute(
        id='land_water_mask',
        title='Land/Water mask',
        bit=5,
        mapping={
            '0': 'Land',
            '1': 'Water',
        }
)

CLOUD_ATTRIBUTES = CloudAttributes(
        id='cloud_ocean',
        title='Cloud/Ocean',
        attributes={
            attribute.id: attribute
            for attribute in (
                CLOUD_MASK,
                FINAL_CLOUD_PLUS_REGION_GROWING,
                FINAL_CLOUD,
                TEST_BAND_4,
                TEST_BAND_5,
                LAND_WATER_MASK,
            )
        }
)
